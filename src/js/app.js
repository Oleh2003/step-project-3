export { appendCard, initUIbtn, getCards };

import {
    Visit,
    VisitCardiologist,
    VisitDentist,
    VisitTherapist,
    createVisitClass,
} from "./classes.js";

import { modalWindow, ageAvarage, closeModal } from "./modal.js";
import { btnCreateLogin, logIn, checkUser } from "../js/btnlogin.js";

//import { getResponce, token, userId } from "./requests.js";
("use strict");
import {
    read_cards,
    del_cards,
    upd_cards,
    new_card,
    get_token,
} from "../js/api_filter.js";

//===DAM===
let data;
let res;
let result;
let token = "41163044-0ead-49e8-9d2f-fd89f869b08c";

function getCards() {
    data = read_cards(); //DAM вивод всіх сфкв
    data.then((result) => {
        res = result;
        console.log(result);

        document.querySelector("#btn1").addEventListener("click", function () {
            console.log(result);
            list_Card(result);
        });

        list_Card(result);
    });
}

function initUIbtn() {
    document.getElementById("btn").addEventListener("click", function () {
        Object.keys(res).forEach(function (key) {
            let id = res[key].id;
            del_cards(id);
        });
    });

    document.querySelectorAll(".search").forEach(function (element) {
        console.log(element);
        element.addEventListener("input", function () {
            const search = document.getElementById("search").value;
            const status1 = document.getElementById("status").value;
            const priority = document.getElementById("priority").value;
            const doc_res = document.getElementById("search-results");
            doc_res.innerHTML = "<br>";
            result = res.filter(
                (item) =>
                    (item.description
                        .toLowerCase()
                        .includes(search.toLowerCase()) ||
                        item.fullName
                            .toLowerCase()
                            .includes(search.toLowerCase())) &&
                    (status1 === "All" || item.status === status1) &&
                    (priority === "All" || item.priority === priority)
            );
            list_Card(result);
        });
    });
}

const createVisitBtn = document.querySelector("#btnCreate");

createVisitBtn.addEventListener("click", function (e) {
    e.preventDefault();
    console.log(e);

    const formBlock = document.createElement("div"); // создание блока формы
    const formWrapper = document.querySelector(".form-wrapper"); // класс блока формы
    formBlock.innerHTML = `
    <div class="modal-container" id="modalContainer">
    <div class="container modal_window">
    <form action=""  class=" form-label" id="form">
    <div class="form-container mb-2">
        <label for="fullName"  class="col-form-label"></label>
        <input name='fullName' type="text" placeholder="Ф.И.О." id='fullName'>
        <br>
        <select class="select mb-2 mt-2" name="select-doctor" id="select-doctor" required="required">
            <option value="">Выберите врача</option>
            <option value="1">Кардиолог</option>
            <option value="2">Стоматолог</option>
            <option value="3">Терапевт</option>
        </select>
        <br>
        <label>
            <input class="mb-2" type="text" placeholder="Цель визита" name='visitPurpose'>
        </label>
        <br>
        <label>
            <input class="mb-2" type="text" placeholder="Краткое описание визита" name='description'>
        </label>
        <br class="br">

        <select class="mb-2" name="priority" id="visit-urgency" required="required">
            <option value="">Срочность визита</option>
            <option value="Normal">Обычная</option>
            <option value="High">Приоритетная</option>
            <option value="Low">Неотложная</option>
        </select>
    </div>  
    <button type="submit" class="form-button btn btn-primary">Создать визит</button>
    </form>
    <button class="btn btn-close close-modal"></button></div>
    </div>
    `;
    const cardBox = document.querySelector(".card-box");
    cardBox.style.display = "none";

    formWrapper.appendChild(formBlock);
    init();
});

read_cards(); //dam Зчитує всі картки
// list_Card(result); //dam Виводить список карток

function init() {
    modalWindow(); // функция вызова модального окна с формой
    createCard(); // функция работы с формой
    closeModal();
}

var bd = {
    title: "Визит к кардиологу",
    id: 17982,
    description: "Новое описание визита",
    doctor: "Cardiologist",
    status: "Done",
    priority: "Low",
    bp: "24",
    age: 23,
    weight: 70,
};

function list_Card(result) {
    const wrapper = document.querySelector(".form-wrapper1");
    wrapper.innerHTML = "";
    let cardContent = "";
    result.forEach((result, index) => {
        cardContent = "";
        const card = document.createElement("div");
        card.classList.add("card");
        for (let key in result) {
            let value = result[key];
            if (key === "fulname") {
                cardContent += "";
            }
            if (key !== "fullName") {
                cardContent += `<p class="hidden-field">${key}: ${value}</p>`;
            } else {
                cardContent += `<p>${key}: ${value}</p>`;
            }
        }
        cardContent +=
            "<button class='show-more'>Показать больше</button>"
        card.innerHTML = cardContent;
        wrapper.appendChild(card);
    });
    initCard();
}

// list_Card(result);
// Создать картоку кнопка =======================================
function appendCard(bd) {
    const wrapper = document.querySelector(".form-wrapper1");
    const card = document.createElement("div");
    card.classList.add("card");
    let cardContent = "";
    // console.log(cardData);
    bd.forEach((field, index) => {
        if (index === 2) {
            cardContent += "<button class='show-more'>Показать больше</button>";
        }
        if (index > 1) {
            cardContent += `<p class="hidden-field">${field.name}: ${field.value}</p>`;
        } else {
            cardContent += `<p>${field.name}: ${field.value}</p>`;
        }
    });
    // console.log(cardContent);
    card.innerHTML = cardContent;
    wrapper.appendChild(card);
    initCard();
}

//var bd = JSON.stringify({ title: 'Визит к кардиологу', description: 'Новое описание визита', doctor: 'Cardiologist', status: 'Done', priority: 'Low', bp: '24', age: 23, weight: 70 });

function initCard() {
    const showMoreBtns = document.querySelectorAll(".show-more");

    showMoreBtns.forEach((btn) => {
        btn.addEventListener("click", function () {
            const hiddenFields =
                this.parentElement.querySelectorAll(".hidden-field");
            hiddenFields.forEach((field) => {
                field.style.display = "block";
            });
            btn.style.display = "none";
        });
    });
    read_cards();
}

const { form1 } = document.forms;
function createCard() {
    const form = document.querySelector("#form");
    form.addEventListener("submit", (event) => {
        event.preventDefault();
        const formData = new FormData(form);
        const values = Object.fromEntries(formData.entries());
        const doctorId = values["select-doctor"];
        const visit = createVisitClass(doctorId, values);
        console.log(visit);
        visit.status = "Open";
        console.log(JSON.stringify(visit));
        new_card(JSON.stringify(visit));
        console.log("hello2");

        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: visit.getFormatedData(),
        }).then((response) => {
            if (response.status !== 200) {
                return;
            }
            appendCard(visit.getCard());
        });
        form.reset(); // обнуление формы после заполнения

        document.querySelector("#modalContainer").style.display = "none";
    });
}
