export { modalWindow, ageAvarage, createFilter, closeModal };

// Добавление полей ====================================================
function modalWindow() {
    const form = document.querySelector(".form-container"); // блок формы
    const formAdditional = document.createElement("div"); // создали див куда добавим контент
    formAdditional.classList.add("form-additional"); // добавляем класс
    const selectDoctor = document.querySelector("#select-doctor"); // переменная селекта выбора врача
    selectDoctor.addEventListener("change", (event) => {
        const data = event.target.value;
        console.log(event.target.value);
        if (data === "1") {
            formAdditional.innerHTML = `
                        <label>
                            <input type="text" placeholder="Обычное давление" required="required" name='pressure'>
                        </label>
                        <br>
                        <label>
                            <input type="text" placeholder="индекс массы тела" required="required" name='massIndex'>
                        </label>
                        <br>
                        <label>
                            <input type="text"
                                placeholder="перенесенные заболевания сердечно-сосудистой системы" required="required" name='pastIllnesses'>
                        </label>
                        <br>
                        <label> Возраст
                        <input type="range" min="1" max="100" value="21" placeholder="возраст" name='age'
                                class="age-range" required="required">
                                <span class="age-span">21</span>
                                лет
                        </label>
                        <br>
                        `; // добавление полей кардиолога
            // console.log(formAdditional);
            form.append(formAdditional);
            ageAvarage();
        } else if (data === "2") {
            formAdditional.innerHTML = `
                <label>
                    <input type="date" placeholder="дата последнего посещения" required="required" name='lastVisitDate'>
                </label>
                <br>`;
            form.append(formAdditional);
        } else if (data === "3") {
            formAdditional.innerHTML = ` 
            <label> Возраст
                <input type="range" min="1" max="100" value="21" placeholder="возраст" name='age'
                    class="age-range" required="required">
                    <span class="age-span">21</span>
                    лет
            </label>`;
            form.append(formAdditional);
            ageAvarage();
        } else {
            formAdditional.innerHTML = ``;
            form.append(formAdditional);
        }
    });
}
// Добавление полей ====================================================

function ageAvarage() {
    // диапазон возраста динамический
    const rangeAge = document.querySelector(".age-range");
    rangeAge.oninput = () => {
        document.querySelector(".age-span").innerHTML =
            document.querySelector(".age-range").value;
    };
}

// Block create filter html ==============================================================
function createFilter() {
    const main = document.querySelector("main");
    const existingFilter = document.querySelector(
        ".container.md-4.mt-4.container-wrap"
    );
    if (existingFilter) {
        existingFilter.remove(); // Видалити попереднє поле пошуку, якщо воно існує
    }

    const filterContainer = document.createElement("section");
    
    filterContainer.innerHTML = `<div class="container mb-4 mt-4">
        <div class="col me-4">
            <label for="search" class="form-label">Заголовок</label>
            <input class="form-control search me-4" list="search" id="search" placeholder="Type to search...">
            <datalist id="search">
                <option value="San Francisco">
                <option value="New York">
            </datalist>
        </div>
        <div class="col me-4">
            <label for="status" class="form-label me-4">Статус</label>
            <select class="form-control search " id="status">
                <option value="All">All</option>
                <option value="Open">Open</option>
                <option value="Done">Done</option>
            </select>
        </div>
        <div class="col me-4">
            <label for="priority" class="form-label">Приоритет</label>
            <select class="form-control search" id="priority">
                <option value="All">All</option>
                <option value="High">High</option>
                <option value="Normal">Normal</option>
                <option value="Low">Low</option>
            </select>
        </div>
        <div class="col button-ds">
            <button class="btn btn-primary" id="btn" style="width: 100px; margin-bottom: 10px;">DELETE</button><br>
            <button class="btn btn-primary" id="btn1" style="width: 100px; margin-bottom: 10px;">STATUS</button><br>
        </div>
        <div id="search-results"></div>
    </div>
    <div>
        <div class="form-wrapper"></div>
        <div class="form-wrapper1"></div>
    </div>`;
    main.appendChild(filterContainer);
}

function closeModal() {
    //+
    const modalContainer = document.querySelector(".modal-container");
    const modal = document.querySelector(".close-modal");
    const submitButton = document.querySelector(".form-button");
    const form = document.querySelector(".modal_window");
    document.addEventListener("click", (e) => {
        if (e.target === modal || e.target == modalContainer) {
            modalContainer.classList.add("modal-container-close");
        } else if (e.target === document.querySelector("#btnCreate")) {
            modalContainer.classList.remove("modal-container-close");
        }
    });
}
