export {
    Visit,
    VisitCardiologist,
    VisitDentist,
    VisitTherapist,
    createVisitClass,
};

class Visit {
    constructor(
        fullName,
        doctor,
        visitPurpose,
        description,
        priority,
        status,
        title
    ) {
        this.fullName = fullName;
        this.doctor = doctor;
        this.visitPurpose = visitPurpose;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.title = title;
    }

    getFormatedData() {
        return JSON.stringify({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            title: this.title,
        });
    }

    getCard() {
        return this.createCard({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
        });
    }
}

// classes ========================================================

class VisitCardiologist extends Visit {
    constructor(
        fullName,
        doctor,
        visitPurpose,
        description,
        priority,
        pressure,
        massIndex,
        pastIllnesses,
        age,
        title
    ) {
        super(fullName, doctor, visitPurpose, description, priority, title);
        this.pressure = pressure; // давление
        this.massIndex = massIndex; // индекс массы
        this.pastIllnesses = pastIllnesses; // перенесенные заболевания
        this.age = age;
    }

    getFormatedData() {
        return JSON.stringify({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            pressure: this.pressure,
            massIndex: this.massIndex,
            pastIllnesses: this.pastIllnesses,
            age: this.age,
            title: this.title,
        });
    }

    getCard() {
        return this.createCard({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            pressure: this.pressure,
            massIndex: this.massIndex,
            pastIllnesses: this.pastIllnesses,
            age: this.age,
        });
    }

    createCard(values) {
        return [
            {
                name: "Ф.И.О",
                value: values.fullName,
            },
            {
                name: "Врач",
                value: "Кардиолог",
            },
            {
                name: "Цель визита",
                value: values.visitPurpose,
            },
            {
                name: "Срочность визита",
                value: values.priority,
            },
            {
                name: "Давление",
                value: values.pressure,
            },
            {
                name: "Краткое описание визита",
                value: values.description,
            },
            {
                name: "Сердечно-сосудистые заболевания",
                value: values.pastIllnesses,
            },
            {
                name: "Возраст",
                value: values.age,
            },
        ];
    }
}

class VisitDentist extends Visit {
    constructor(
        fullName,
        doctor,
        visitPurpose,
        description,
        priority,
        lastVisitDate,
        title
    ) {
        super(fullName, doctor, visitPurpose, description, priority, title);
        this.lastVisitDate = lastVisitDate; // послежний визит
    }

    getFormatedData() {
        return JSON.stringify({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            lastVisitDate: this.lastVisitDate,
            title: this.title,
        });
    }

    getCard() {
        return this.createCard({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            lastVisitDate: this.lastVisitDate,
        });
    }

    createCard(values) {
        return [
            {
                name: "Ф.И.О",
                value: values.fullName,
            },
            {
                name: "Врач",
                value: "Стоматолог",
            },
            {
                name: "Цель визита",
                value: values.visitPurpose,
            },
            {
                name: "Срочность визита",
                value: values.priority,
            },
            {
                name: "Краткое описание визита",
                value: values.description,
            },
            {
                name: "Последняя дата визита",
                value: values.lastVisitDate,
            },
        ];
    }
}

class VisitTherapist extends Visit {
    constructor(
        fullName,
        doctor,
        visitPurpose,
        description,
        priority,
        age,
        title
    ) {
        super(fullName, doctor, visitPurpose, description, priority, title);
        this.age = age;
    }

    getFormatedData() {
        return JSON.stringify({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            age: this.age,
            title: this.title,
        });
    }

    getCard() {
        return this.createCard({
            fullName: this.fullName,
            doctor: this.doctor,
            visitPurpose: this.visitPurpose,
            description: this.description,
            priority: this.priority,
            age: this.age,
        });
    }

    createCard(values) {
        return [
            {
                name: "Ф.И.О",
                value: values.fullName,
            },
            {
                name: "Врач",
                value: "Стоматолог",
            },
            {
                name: "Цель визита",
                value: values.visitPurpose,
            },
            {
                name: "Срочность визита",
                value: values.priority,
            },
            {
                name: "Краткое описание визита",
                value: values.description,
            },
            {
                name: "Возраст",
                value: values.age,
            },
        ];
    }
}

function createVisitClass(doctorId, values) {
    console.log(doctorId, values);
    switch (doctorId) {
        case "1":
            const visitCardiologist = new VisitCardiologist(
                values.fullName,
                values.doctor,
                values.visitPurpose,
                values.description,
                values.priority,
                values.pressure,
                values.massIndex,
                values.pastIllnesses,
                values.age
            );
            console.log(visitCardiologist);
            return visitCardiologist;

        case "2":
            const visitDentist = new VisitDentist(
                values.fullName,
                values.doctor,
                values.visitPurpose,
                values.description,
                values.priority,
                values.lastVisitDate
            );
            console.log(visitDentist);
            return visitDentist;

        case "3":
            const visitTherapist = new VisitTherapist(
                values.fullName,
                values.doctor,
                values.visitPurpose,
                values.description,
                values.priority,
                values.age
            );
            console.log(visitTherapist);
            return visitTherapist;

        default:
            break;
    }
}
