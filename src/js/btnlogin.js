"use strict";
import {
    createFilter
} from "./modal.js";
import {
    get_token
} from "../js/api_filter.js";
import {
    initUIbtn,
    getCards
} from "./app.js";
const formHeader = document.querySelector("#formHeader")
const btnLogin = document.querySelector("#btnLogin");
const cardBox = document.querySelector(".card-box");
const buttonCreateVizit = document.querySelector("#btnCreate");
const buttonExit = document.querySelector("#btnExit");

window.addEventListener("load", checkUser)

export function checkUser() {
    const token = localStorage.getItem("token"); /* знаходить токен у локальному сховищі */
    if (token) {
        btnLogin.style.display = "none";
        buttonCreateVizit.style.display = "block";
        buttonExit.style.display = "block";
        createFilter();
        initUIbtn();
        if (localStorage.getItem("cards")) {
            const savedCards = JSON.parse(localStorage.getItem("cards")); /* отримуємо збережені дані (картки) з локального сховища */
            renderCards(savedCards); /* відображаємо збережені картки на сторінці */
        } else {
            getCards(); /* отримуємо нові картки, якщо збережених немає */
        }
    } else {
        btnLogin.style.display = "block";
        buttonCreateVizit.style.display = "none";
    }
}

buttonExit.addEventListener("click", (e) => {
    e.preventDefault();
    localStorage.removeItem("token"); /* видаляє токен з локального сховища */
    localStorage.removeItem("cards"); /* видаляє збережені дані (картки) з локального сховища */
    btnLogin.style.display = "block";
    buttonCreateVizit.style.display = "none";
    buttonExit.style.display = "none";
    document.querySelector("section").remove();
});

export function btnCreateLogin() {
    formHeader.addEventListener("click", (e) => {
        e.preventDefault();
        if (e.target.id === "btnLogin") {
            cardBox.style.display = "block";
            logIn();
            console.log(e.target);
        }
    });
}

btnCreateLogin();

const formLogin = document.querySelector("#formLogin");

export function logIn() {
    formLogin.addEventListener("submit", (e) => {
        e.preventDefault();
        const email = e.target.querySelector("#email").value;
        const password = e.target.querySelector("#password").value;
        if (email && password) {
            get_token(email, password).then((data) => {
                localStorage.setItem("token", data); /* зберігаємо токен у локальному сховищі */
                document.querySelector("#email").value = "";
                document.querySelector("#password").value = "";

                cardBox.style.display = "none";
                checkUser();
            });
        }
    });
}