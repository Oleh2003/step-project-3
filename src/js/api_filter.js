var token = "82693eac-351e-4d82-8c05-6e5a20f9e6ab"; // записать заниение регистрации
var id = "170993";
var bd = JSON.stringify({
    title: "Визит к кардиологу",
    description: "Новое описание визита",
    doctor: "Cardiologist",
    status: "Done",
    priority: "Low",
    bp: "24",
    age: 23,
    weight: 70,
});

export async function get_token(email, password) {
    const res = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            // 'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            email: email,
            password: password,
        }),
    });
    const data = await res.text();
    // localStorage.setItem("token", data);
    console.log(data);
    return data;
}

export async function read_cards() {
    const res = await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    });
    if (!res.ok) {
        const message = "Error:" + res.status;
        throw new Error(message);
    }
    const result = await res.json();
    return result;
}

export async function del_cards(id) {
    const res = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    });
}

export async function upd_cards(id, bd) {
    const res = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: bd,
    });
}

//f_del(170993);

//f_upd(id,bd);
export async function new_card(bd) {
    console.log("requets", bd);
    const result = fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        body: bd,
    })
        .then((response) => response.json())
        .then((response) => console.log(response));
}

// new_card(bd);
